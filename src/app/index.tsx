import * as React from 'react';
import '@patternfly/react-core/dist/styles/base.css';

// Local CSS changes
import '@app/merge.css';

import { BrowserRouter as Router } from 'react-router-dom';
import { AppRoutes } from '@app/routes';
import AuthProvider from '@app/lib/AuthProvider';
import ErrorBoundary from '@app/lib/ErrorBoundary';
import { CustomTranslations, CustomLanguageFormats, IntlProvider, /* ThemeProvider, */ locales } from '@ory/elements';

// required styles for Ory Elements
import '@ory/elements/assets/normalize.css';
import '@ory/elements/style.css';

import '@app/lib/i18n';

const App: React.FunctionComponent = () => {
  return <Main />;
};

const customTranslations: CustomLanguageFormats = {
  en: {
    ...locales.en,
    'login.title': 'Login',
    'identities.messages.1070004': 'Username',
  },
  nl: {
    ...locales.nl,
    'login.title': 'Inloggen',
    'identities.messages.1070004': 'Username',
  },
  af: {
    // merging English since no default Afrikaans translations are available
    ...locales.en,
    'login.title': 'Meld aan',
    'identities.messages.1070004': 'E-posadres',
  },
};

// left as an example. We use css to set fonts for ory elements
// const themeOverrides = {
//   fontFamily: 'Verdana',
//   fontFamilyMono: 'Courier New',
//   fontStyle: 'normal',
// };

const Main: React.FunctionComponent = () => (
  <React.StrictMode>
    {/* <ThemeProvider themeOverrides={themeOverrides}> */}
    <IntlProvider<CustomTranslations> locale="en" defaultLocale="en" customTranslations={customTranslations}>
      <AuthProvider>
        <Router>
          <ErrorBoundary>
            <AppRoutes />
          </ErrorBoundary>
        </Router>
      </AuthProvider>
    </IntlProvider>
    {/* </ThemeProvider> */}
  </React.StrictMode>
);

export default App;
