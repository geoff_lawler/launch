import * as React from 'react';
import { AuthContext } from '@app/lib/AuthProvider';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { Avatar, Dropdown, DropdownToggle, DropdownItem, Button, Icon } from '@patternfly/react-core';
import CaretDownIcon from '@patternfly/react-icons/dist/esm/icons/caret-down-icon';
import UserIcon from '@patternfly/react-icons/dist/esm/icons/user-icon';
import { Link, useHistory } from 'react-router-dom';
import { AxiosError } from 'axios';
import ory from '@app/lib/OrySdk';
import { GetUserRequest, GetUserResponse } from '@mergetb/api/portal/v1/workspace_types';
import ExclamationCircleIcon from '@patternfly/react-icons/dist/esm/icons/exclamation-circle-icon';

const AccountMenu: React.FunctionComponent = () => {
  const { auth } = React.useContext(GeneralSettingsContext);
  const { api } = React.useContext(GeneralSettingsContext);
  const { isAuthenticated, identity, setAdmin, isAdmin } = React.useContext(AuthContext);
  const [isOpen, setIsOpen] = React.useState(false);

  const logout = CreateLogoutHandler([isAuthenticated]);

  const dropdownItems = [
    <DropdownItem key="settings" component={<Link to="/settings">Modify Identity Profile</Link>} />,
    <DropdownItem
      key="logout"
      component={
        <Button onClick={logout} variant="link">
          Logout
        </Button>
      }
    />,
  ];

  const icon = identity?.traits.picture ? (
    <Avatar src={identity?.traits.picture} alt="avatar" size="md" />
  ) : (
    <UserIcon />
  );

  const reddot = isAdmin ? (
    <Icon status="danger">
      <ExclamationCircleIcon />
    </Icon>
  ) : (
    <></>
  );

  // read User data and set admin in auth context.
  React.useEffect(() => {
    if (identity === undefined || identity.traits.username === '') {
      return;
    }

    fetch(api + '/user/' + identity.traits.username, {
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((resp) => {
      if (resp.ok) {
        resp.json().then((data) => {
          const user = GetUserResponse.fromJSON(data);
          if (user.user !== undefined) {
            return setAdmin(user.user.admin);
          }
        });
      }
    });
  }, [setAdmin, isAuthenticated, identity, api]);

  return (
    <React.Fragment>
      {isAuthenticated ? (
        <Dropdown
          onSelect={() => setIsOpen(!isOpen)}
          isOpen={isOpen}
          isPlain
          isFullHeight
          position={'right'}
          toggle={
            <DropdownToggle
              id="user-settings-dropdown"
              onToggle={(next) => setIsOpen(next)}
              toggleIndicator={CaretDownIcon}
              icon={icon}
            >
              {identity?.traits.username}
            </DropdownToggle>
          }
          dropdownItems={dropdownItems}
        />
      ) : (
        <Link to="/login">Login</Link>
      )}
      {reddot}
    </React.Fragment>
  );
};

// Return a function that logs the user out.
const CreateLogoutHandler: React.FC = (deps?: React.DependencyList) => {
  const [logoutToken, setLogoutToken] = React.useState<string>('');
  const history = useHistory();
  const { setSession } = React.useContext(AuthContext);

  React.useEffect(() => {
    ory
      .createBrowserLogoutFlow()
      .then(({ data }) => {
        setLogoutToken(data.logout_token);
      })
      .catch((err: AxiosError) => {
        switch (err.response?.status) {
          case 401:
            // do nothing, the user is not logged in
            return;
        }

        // Something else happened!
        return Promise.reject(err);
      });
  }, deps);

  return () => {
    if (logoutToken) {
      ory.updateLogoutFlow({ token: logoutToken }).then(() => {
        setSession(undefined);
        history.push('/login');
        history.go(0);
      });
    }
  };
};

export { AccountMenu };
