import * as React from 'react';
import { useHistory } from 'react-router-dom';
import {
  RegistrationFlow,
  UiNode,
  UiText,
  UiTextTypeEnum,
  UpdateRegistrationFlowBody,
  UpdateRegistrationFlowWithPasswordMethod,
} from '@ory/client';
import {
  Alert,
  AlertGroup,
  LoginPage,
  ListVariant,
  EmptyState,
  EmptyStateIcon,
  Spinner,
  Form,
  FormGroup,
  FormSelect,
  FormSelectOption,
  TextInput,
  Stack,
  StackItem,
  Card,
  CardTitle,
  CardBody,
} from '@patternfly/react-core';
import ory, { sdkError } from '@app/lib/OrySdk';
import { UserAuthCard } from '@ory/elements';
import { useFetch } from 'use-http';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { GetUserConfigurationsResponse } from '@mergetb/api/portal/v1/workspace_types';
import { RegisterRequest } from '@mergetb/api/portal/v1/identity_types';
import ExclamationCircleIcon from '@patternfly/react-icons/dist/esm/icons/exclamation-circle-icon';

import launchLogo from '@app/bgimages/launch-logo.svg';
import bgImage1200 from '@app/bgimages/bg-1200.png';
import bgImage768 from '@app/bgimages/bg-768.png';
import bgImage768_2x from '@app/bgimages/bg-768.png';
import bgImage576 from '@app/bgimages/bg-576.png';
import bgImage576_2x from '@app/bgimages/bg-576.png';

const Register: React.FunctionComponent = () => {
  const [flow, setFlow] = React.useState<RegistrationFlow>();
  const [institution, setInstition] = React.useState<string>('');
  const [otherinst, setOtherInst] = React.useState<string>('');
  const [country, setCountry] = React.useState<string>();
  const [usstate, setUsstate] = React.useState<string>();
  const [accountCategory, setAccountCategory] = React.useState<string>('');
  const [fullname, setFullname] = React.useState<string>('');
  const [fullnameError, setFullnameError] = React.useState<string>('');
  const [regError, setRegError] = React.useState<string>('');

  const params = new URLSearchParams(document.location.search);
  const returnTo = params.get('return_to');
  const loginChallenge = params.get('login_challenge');

  const history = useHistory();
  const conf = React.useContext(GeneralSettingsContext);

  const options = {
    credentials: 'include',
    cachePolicy: 'no-cache',
  };

  const { data: userConfigData } = useFetch(conf.api + '/configurations/user', options, []);

  // Get the flow based on the flowId in the URL (.e.g redirect to this page after flow initialized)
  const getFlow = React.useCallback(
    (flowId: string) =>
      ory
        // the flow data contains the form fields, error messages and csrf token
        .getRegistrationFlow({ id: flowId })
        .then(({ data: flow }) => setFlow(flow))
        .catch(sdkErrorHandler),
    []
  );

  // initialize the sdkError for generic handling of errors
  const sdkErrorHandler = sdkError(getFlow, setFlow, '/registration', true);

  // create a new registration flow
  const createFlow = () => {
    ory
      // we don't need to specify the return_to here since we are building an SPA. In server-side browser flows we would need to specify the return_to
      .createBrowserRegistrationFlow({
        ...(returnTo && { returnTo: returnTo }),
        ...(loginChallenge && { loginChallenge: loginChallenge }),
      })
      .then(({ data: flow }) => {
        // Update URI query params to include flow id
        const params = new URLSearchParams({ ['flow']: flow.id });
        history.replace({ pathname: location.pathname, search: params.toString() });
        // Set the flow data
        setFlow(flow);
      })
      .catch(sdkErrorHandler);
  };

  // read and parse userConfigData once is shows up.
  const userConf = React.useMemo(() => {
    if (userConfigData) {
      if (Object.prototype.hasOwnProperty.call(userConfigData, 'institutions')) {
        const c = GetUserConfigurationsResponse.fromJSON(userConfigData);
        setInstition(c.institutions[0]);
        setAccountCategory(c.categories[0]);
        setCountry('United States');
        setUsstate(c.usstates[0].name);
        return c;
      }
    }
    return undefined;
  }, [userConfigData]);

  const onRegistrationSubmit = (body: UpdateRegistrationFlowBody) => {
    // something unexpected went wrong and the flow was not set
    if (!flow) return history.push('/registration', { replace: true });
    // Make sure our fields are ok
    if (fullname === '') {
      setFullnameError('Full Name is a required field');
      return Promise.reject();
    }

    ory
      .updateRegistrationFlow({
        flow: flow.id,
        updateRegistrationFlowBody: body,
      })
      .then(({ data }) => {
        // At this point we have an kratos ID. So now we create the portal user data.
        console.log('body', body);
        const req: RegisterRequest = {
          username: body['traits.username'],
          email: body['traits.email'],
          name: fullname,
          password: body.password,
          institution: institution === 'Other' ? otherinst : institution,
          category: accountCategory,
          usstate: usstate ? usstate : '',
          country: country ? country : '',
          admin: false,
          traits: undefined,
        };

        fetch(conf.api + '/register', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(req),
        }).then((resp) => {
          console.log('register resp', resp);
          if (!resp.ok) {
            return resp.json().then((j) => {
              console.log('error message', j.message);
              throw new Error(j.message);
            });
          } else {
            history.push(flow?.return_to || '/login');
          }
          return Promise.resolve();
        });

        // continue with kratos handling.
        if ('continue_with' in data) {
          for (const cw of data.continue_with ?? []) {
            if (cw.action === 'show_verification_ui') {
              const search = new URLSearchParams();
              search.set('flow', cw.flow.id);
              history.push(
                {
                  pathname: '/verification',
                  search: search.toString(),
                },
                { replace: true }
              );
              return;
            }
          }
        }

        // we successfully submitted the login flow, so lets redirect to the dashboard
        history.push('/', { replace: true });
      })
      .catch(sdkErrorHandler)
      .catch((err) => {
        console.log('got non-Kratos error', err);
        if (err.response?.status === 400) {
          // put our nodes back in before continuing.
          const f = err.response?.data;
          setFlow(f);
        }
        err.json().then((j) => {
          setRegError(j.message);
          // TODO: Need to unregister user here if merge api error.
        });
      });
  };

  // create the flow
  React.useEffect(() => {
    // we might redirect to this page after the flow is initialized, so we check for the flowId in the URL
    const flowId = params.get('flow');
    // the flow already exists
    if (flowId) {
      getFlow(flowId).catch(createFlow); // if for some reason the flow has expired, we need to get a new one
      return;
    }
    // we assume there was no flow, so we create a new one
    createFlow();
  }, []);

  const images = {
    lg: bgImage1200,
    sm: bgImage768,
    sm2x: bgImage768_2x,
    xs: bgImage576,
    xs2x: bgImage576_2x,
  };

  const handleFullnameChange = (e) => {
    setFullname(e);
    setFullnameError('');
  };

  const handleSetCountry = (e) => {
    setCountry(e);

    if (e !== 'United States') {
      setUsstate('');
    }
  };

  return (
    <React.Fragment>
      {regError !== '' && (
        <AlertGroup isToast>
          <Alert variant="danger" title={regError} />
        </AlertGroup>
      )}
      <LoginPage
        loginTitle={'Register for a Merge account'}
        // footerListItems={footerListItems}
        footerListVariants={ListVariant.inline}
        brandImgSrc={launchLogo}
        brandImgAlt="Merge Launch"
        textContent={
          "Once you have registered, you will need to contact a Merge Portal operator or your organization's Portal adminstrator to initialize and activate your account. Until your account is approved you will be able to login, but have access to no Merge resources."
        }
        backgroundImgSrc={images}
      >
        {(() => {
          if (flow?.id) {
            return (
              <Stack hasGutter>
                <StackItem>
                  <Card>
                    <CardTitle>Merge Account Fields</CardTitle>
                    <CardBody>
                      <Form>
                        <FormGroup
                          helperTextInvalid={fullnameError}
                          helperTextInvalidIcon={fullnameError !== '' && <ExclamationCircleIcon />}
                          label="Full Name"
                          fieldId="fullname"
                          isRequired
                          validated={fullnameError !== '' ? 'error' : 'success'}
                        >
                          <TextInput
                            isRequired
                            onChange={(e) => handleFullnameChange(e)}
                            type="text"
                            value={fullname}
                            aria-label={'Full Name'}
                            label={'Full Name'}
                          />
                        </FormGroup>
                        <FormGroup label="Institution" fieldId="institution" isRequired>
                          <FormSelect value={institution} onChange={setInstition} aria-label="Institution">
                            {userConf?.institutions.map((e, i) => (
                              <FormSelectOption key={i} value={e} label={e} />
                            ))}
                          </FormSelect>
                        </FormGroup>
                        {institution === 'Other' && (
                          <FormGroup label="Other Institution" fieldId="otherinst" isRequired>
                            <TextInput
                              isRequired
                              value={otherinst}
                              onChange={setOtherInst}
                              aria-label="Other Institution"
                            />
                          </FormGroup>
                        )}
                        <FormGroup label="Account Category" fieldId="accountcategory" isRequired>
                          <FormSelect
                            value={accountCategory}
                            onChange={setAccountCategory}
                            aria-label="Account Category"
                          >
                            {userConf?.categories.map((e, i) => (
                              <FormSelectOption key={i} value={e} label={e} />
                            ))}
                          </FormSelect>
                        </FormGroup>
                        <FormGroup label="Country" fieldId="country" isRequired>
                          <FormSelect value={country} onChange={handleSetCountry} aria-label="Country">
                            {userConf?.countries.map((e, i) => (
                              <FormSelectOption key={i} value={e.name} label={e.name} />
                            ))}
                          </FormSelect>
                        </FormGroup>
                        {country === 'United States' && (
                          <FormGroup label="US State" fieldId="usstate" isRequired>
                            <FormSelect value={usstate} onChange={setUsstate} aria-label="US State">
                              {userConf?.usstates.map((e, i) => (
                                <FormSelectOption key={i} value={e.name} label={e.name} />
                              ))}
                            </FormSelect>
                          </FormGroup>
                        )}
                      </Form>
                    </CardBody>
                  </Card>
                </StackItem>
                <StackItem>
                  <UserAuthCard
                    title={'Identity Account Fields'}
                    flowType={'registration'}
                    onSubmit={({ body }) => onRegistrationSubmit(body as UpdateRegistrationFlowBody)}
                    flow={flow}
                  />
                </StackItem>
              </Stack>
            );
          }
          return (
            <EmptyState>
              <EmptyStateIcon variant="container" component={Spinner} />
              Contacting Authorization Server
            </EmptyState>
          );
        })()}
      </LoginPage>
    </React.Fragment>
  );
};

export { Register };
