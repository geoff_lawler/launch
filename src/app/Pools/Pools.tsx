import * as React from 'react';
import { PageSection, Title } from '@patternfly/react-core';

const Pools: React.FunctionComponent = () => (
  <PageSection>
    <Title headingLevel="h1" size="lg">
      Pool Management
    </Title>
  </PageSection>
);

export { Pools };
