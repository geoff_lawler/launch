import * as React from 'react';
import { PageSection, Card, CardTitle, CardBody } from '@patternfly/react-core';
import { useLocation } from 'react-router-dom';

interface ErrorProps {
  code: integer;
  message: string;
  details: {};
}

const ErrorPage: React.FC<ErrorProps> = ({ code, message, details }) => {
  return (
    <React.Fragment>
      <PageSection>
        <Card>
          <CardTitle>Error</CardTitle>
          <CardBody>
            Message: {message}
            <br />
            Details: <pre>{JSON.stringify(details, null, 2)}</pre>
          </CardBody>
        </Card>
      </PageSection>
    </React.Fragment>
  );
};

export { ErrorPage };
