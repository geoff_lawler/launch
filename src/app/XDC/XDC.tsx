import * as React from 'react';
import { useParams, Link } from 'react-router-dom';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { useFetch } from 'use-http';
import { AuthContext } from '@app/lib/AuthProvider';
import {
  PageSection,
  Breadcrumb,
  BreadcrumbItem,
  Card,
  CardHeader,
  CardBody,
  Spinner,
  Bullseye,
  Alert,
  GridItem,
  Grid,
  CardTitle,
} from '@patternfly/react-core';
import {
  GetUserResponse,
  accessModeToJSON,
  userStateToJSON,
  User as PortalUser,
  AccessMode,
  AccessModeUpdate,
  UpdateUserRequest,
} from '@mergetb/api/portal/v1/workspace_types';
import { TableComposable, Thead, Tbody, Tr, Td, Th, ActionsColumn, IAction } from '@patternfly/react-table';
import { TaskStatusTable } from '@app/lib/TaskStatusTable';
import { XDCs } from '@app/XDCs/XDCs'

type XDCProp = {
  pid: string | undefined;
  xid: string | undefined;
};

const XDC: React.FunctionComponent<XDCProp> = ({ pid, xid }) => {
  const { pid } = pid === undefined ? useParams() : pid;
  const { xid } = xid === undefined ? useParams() : xid;
  const { api } = React.useContext(GeneralSettingsContext);

  const [reload, setReload] = React.useState(0);

  const options = { credentials: 'include', cachePolicy: 'no-cache' };
  const { loading, error, data } = useFetch(st_url, options, [reload]);

  const st_url = api + '/xdc/instance/' + xid + "/" + pid + '?statusMS=-1';
  const st_getter = (data) => {
    return data.status;
  };

  const xdc = React.useMemo(() => {
    if (data) {
      if (Object.prototype.hasOwnProperty.call(data, 'xdc')) {
        return data.xdc;
      }
    }
    return undefined;
  }, [data]);

  const crumbs = (
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem to="/project">Projects</BreadcrumbItem>
        <BreadcrumbItem to={'/project/' + pid}>{pid}</BreadcrumbItem>
        <BreadcrumbItem to="/xdcs">XDCs</BreadcrumbItem>
        <BreadcrumbItem>
          {xid}
        </BreadcrumbItem>
      </Breadcrumb>
    </PageSection>
  );

  return (
    <React.Fragment>
      {crumbs}
      <PageSection>
        <Grid hasGutter>
        <GridItem>
            <Card id="xdcCard">
              <CardHeader>
                <CardTitle id="xdcCardTitle">XDC Details</CardTitle>
              </CardHeader>
                <CardBody>
                    <XDCs
                        filter={true}
                        fpid={pid}
                        fxid={xid}
                    />
                </CardBody>
            </Card>
          </GridItem>
          <GridItem>
            <Card id="statusCard">
              <CardHeader>
                <CardTitle id="statusCardTitle">Status</CardTitle>
              </CardHeader>
                <CardBody>
                  <TaskStatusTable
                        kind={xid + '-tst'}
                        url={st_url}
                        getter={st_getter}
                        ongoingfrequency={2000}
                        completedfrequency={60000}
                        scalingfrequency={1.0 / 10.0}
                      />
                </CardBody>
            </Card>
          </GridItem>
        </Grid>
      </PageSection>
    </React.Fragment>
  );
};

export { XDC };
